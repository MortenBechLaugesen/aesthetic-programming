let x = 0;
let y = 0;
let spacing = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(100,45,190);
  frameRate(30);
}

function draw() {
  stroke(255,204,0,150);
  //changed the thickness of the lines
  strokeWeight(4);
  //changed how often you get backslash from 0.5 -> 0.2
  if (random(1) < 0.2) {
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
