### README for miniX10

![](minix10OOPflow.png)

![](minix10dataflow.png)

# What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?

In group:
Creating a flowchart is a method to elaborate your program and clarify the practical and conceptual aspects of the algorithm within.
“Flowcharts, “Flow of the chart →  chart of the flow”, have been considered a fundamental explanatory tool since the early days of computer programming. One of their common uses is to illustrate computational operations and data processing for programming by “converting the numerical method into a series of steps.”” (Soon & Cox 2020 p. 214)
The difficulties involved within creating a flowchart and keeping things simple at the communations level whilst maintaining the complexity is to translate everything from a coding language to the spoken language. It is almost like translating a german text in highschool for the first time. 
“The diagram is an “image of thought,” in which thinking does not consist of problem-solving but — on the contrary — problem-posing.” (Soon and Cox 2020, p. 221)  

Individual:
When trying to simplify something like a description of how something complex works, it can be very hard to do so without truly understanding what one is trying to simplify. I find that flowcharts can help bridge some of this difficulty; It let me even better understand code that I thought I understood somewhat decently beforehand.
Trying to communicate on a simple level becomes even harder in my opinion if what should be communicated haven’t been created yet. For example trying to discuss and/or explain a program before creating the program, this is also a gap I find flowcharts help bridge.


# What are the technical challenges facing the two ideas and how are you going to address these?

In group:
Making the flowcharts and generating different ideas for this week's MiniX, were very different from just making the program to begin with. During the making of each flowchart we were talking about how we were going to make each program, and which syntaxes we were going to use. We weren't quite sure of how to actually make each program,  but were looking around trying to figure out if the programs were even possible for us to make. We found some syntaxes, and some games that had some similarities to what we wanted, and from there we made the flowcharts knowing that the programs are possible to make, even though we are not sure yet, how to technically make them.

Individual:
Some of the technical challenges we are facing for our two ideas are:
For our OOP idea we know somewhat what we would like to make, though this isn’t set in stone since it depends on if we can make it; We have discussed some of the technical challenges and have come to the conclusion that we won’t know if we can make exactly what we want before actually trying.
For our data capture we want our program to take pictures and show them to the user, we aren’t yet sure how to do this.


# In which ways are the individual and the group flowcharts you produced useful?

In group:
The flowcharts can be useful as a tool to communicate how you envision your idea. When making the group flowchart we also found that the flowcharts helped make clear what technical knowhow we need(ed) to create the programs we envision. While making clear what difficulties we would have technically, the flowchart also made clear how our ideas maybe weren’t as complicated as we first imagined. The flowcharts allowed us to quickly get an overview of our ideas and see if the ideas were possible for us.
Individual:
Creating the flowcharts in the group has helped us to find something we would like to make for our final project. They also serve as a form of draft for our final project. They almost serve as a framework to build out from when we have to create the final project, it isn’t so detailed as to say how to make the project, but is also isn’t so bare that it can’t help us at all:
”As we discussed in previous chapters, programming is a form of abstraction that requires the selection of important details in which the implementation embodies the programmers’ thinking and decision-making processes.” (Soon and Cox 2020, p. 222)

---

## Individual flowchart

For my individual flowchart I chose to make mine around my miniX7 assignment. I chose this one because I found it to be my most complicated miniX. This miniX relies on a Daniel Shiffmann video, but through following the video, making my own adjustments and now the flowchart I have a better and better understanding of the code. The flowchart helped me understand the code better and especially how the code works together, since the program has my different syntax.

![](minix10individualflow.png)
