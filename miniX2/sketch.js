let fillVal = 126;
let angle = 0;
let x = 145;
let y = 180;
let liquidcolor = 255;
let fillval2 = 126;

function setup() {
  createCanvas(1500,1500);
  angleMode(DEGREES);
}

// Emoji 1
function draw() {
  fill(0);
  textSize(20);
  text("Up arrow", 160,340);
  // E1 head
  fill(fillVal);
  ellipse(200,200,200,200);
  // E1 left eyes
  fill(255);
  ellipse(160,170,60,60);
  // E1 right eye
  fill(255);
  ellipse(240,170,60,60);
  // E1 mouth
  fill(255);
  ellipse(200,250,120,30);
  //E1 right pupil
  fill(0);
  ellipse(255,160,20,20);
  //E1 left pupil
  push();
  fill(0);
  translate(x,y);
  rotate(angle);
  angle = angle + 5;
  ellipse(0,0,20,22);
  pop();

// Emoji 2
  push();
  noFill();
  strokeWeight(5);
  // E2 liquid
  push();
  fill(fillval2);
  triangle(400,150,600,150,500,350);
  pop();
  // E2 glass
  line(375,100,500,350);
  line(625,100,500,350);
  line(375,100,625,100);
  line(500,350,500,550);
  ellipse(499,550,120,55);
  pop();
}


// E1 farve ændring
function keyPressed() {
  if (keyCode === UP_ARROW) {
    fillVal = color(random(0,255),random(0,255),random(0,255));
  } else if (keyCode === DOWN_ARROW) {
    fillVal = color(random(0,255),random(0,255),random(0,255));
  }
  return false; // prevent default. Hvad betyder det her?
}

//E2 farve ændring
function mousePressed() {
  fillval2 = color(random(0,255),random(0,255),random(0,255));
}
