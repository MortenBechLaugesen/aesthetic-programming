# README for miniX2

[See my code!](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX2/sketch.js)

[See my project!](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX2/)

![](screenshotminiX2.png)


---

### What I have produced
I have produced a project of two emojis with very similar functions. Both emojis change color;
One when the up and down arrows are pressed and one when the mouse is pressed.

The first emoji is a somewhat standard looking smiley face with a round head, big eyes and a big mouth.
I have created this emoji very simple, so that it does not portray stereotypical features as particular people or cultures.
I placed the pupils pointing away from each other to even further make it seem caricature-ish as more so a funny take so the emoji it self is not the focus.
The focus of the emoji is that it can change color. These colors are on a random RGB scale and most of the time will not look like human skintones, though sometimes it can hit a color that could be a human skintone.

The second emoji is a typical martini glass. I have created it using basic shapes like triangles, ellipses and lines.
It functions very much like the first emoji. Its function is changing color. The major difference is just that the glass changes color with mouse presses and the face with the up and down arrows.

### What I have learned
In the process of creating this project i have leaned different things:
I have learned about boolian statements, although i have'nt really put it into practice in the final itteration of my project.
I have become more accustomed to and comfortable with the use and propoties af variables. I have used several variables throughout, although some i decided not to use after trying it in different circumstances.
I have learned the function of push and pop and there incredible usefulness.

### What I have used
I have used:
-Variables (let)
-Different functions (draw, setup, keyPressed, mousePressed)
-Shapes and geomitries (line, ellipse, triangle)
-Boolian statements (if, else)

---

### Social and cultural context
When creating my emojis i at first just tried having some fun with different syntax. But when reading the assigned reading, especially around the use of colors in emojis i thought that it could be a little more meaningful to work with this as a overarching thought.
This i why i have made two very different emojis with very similar functions. The face changing color can be seen as very critical in the discussion of inclusivity  when talking emojis.
On the other hand the glass changing color can be seen as something fun.
The functions are the same but the meaning and thought behind are very much on opposite sites of the discussion.
And even though my smiley most of the time wont hit a skintone like color, it can, which in it self can be seen as inclusive or as not inclusive enough since it cant hit all skintone like colors. 

---

### References

[Martini glass](https://openprocessing.org/sketch/989505)

[Color change that got me thinking about colorchaning emojis in the first place](https://openprocessing.org/sketch/608213)
