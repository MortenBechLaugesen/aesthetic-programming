### README for miniX7

[See my code!](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX7/sketch.js)

[See my project!](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX7/)

![](miniX7screenshot.png)


---

### How my game and it's objects work

My game is a take on the classic game 'snake'. I followed a Daniel Shiffman coding challenge about the snake game and the made some of my own iterations after. When looking at the functionality of the game, it is very much like any other snake game. The snake starts of as being one part long and when you controle it with the arrow keys to food on the screen, it becomes one part longer for each food eaten. What sets my snake game apart from other snake games is the visual. In my snake game, the "snake" is a sun and the food is a raindrop. Whenever the sun gets to the raindrop the raindrop evaporates and it results in more sun. This visual came to because I was tired of the bad weather around the days i worked on this game. 
The only small problem is that after changing the snake and food from p5 elements to my own pngs, the eating happens on the very top of the raindrop, so the objects are a little displaced compared to how they would have been as p5 elements.

---

The objects are the snake as said above which moves by pressing the arrow keys and the raindrops which spawns randomly on the canvas an gets eaten by the snake and the find a new spot on the canvas to spawn anew. I made the snake by creating a class called Snake with some different methods. The methods of the Snake is:

- grow(); this makes the snake grow by 1 part when eating food
- eat(); this allows the snake to eat food when they overlap
- endGame(); this ends the game if the snake collides with itself or the walls of the canvas
- show(); this displays the snake
- setDir(); sets the snake's heading
- update(); shifts a part out of the snakes array, copies it and places it back so the parts of the snake does not overlap, because if they overlap the game is overlap

---

I followed the Shiffman video to familiarise myself better with OOP (Object oriented programming) and it's components. By following along, changing stuff, playing with the code i better understood the different syntax of OOP like classes.

By choosing to use a sun as the snake there is a need for some abstraction. Suns in real life does not have properties that allows them to move in different directions or eat water to multiply. On the other hand, since saying that the sun evaporates the raindrops, in this instance, the need for abstraction is not high since this is a property of both rain and the sun. I haven't used objects that heavily defies their real life properties.

---

I don't truly know how snake can be connected to a wider cultural context. But if I try to think abstract, one might say my game can be a take on climate change. If I changed the raindrop to an ice-cube this might have been a clear linkage. In real life, situations like climate change might be abstracted. In food production of something Co2 heavy the manufacturer properly doesn't want to disclose or display the effects the product has on the climate.

---

### References

[The Coding Train, Daniel Shiffman, Snake Game](https://www.youtube.com/watch?v=OMoVcohRgZA)
