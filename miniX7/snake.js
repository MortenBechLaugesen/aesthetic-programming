class Snake {
  constructor() {
    this.body = [];
    this.body[0] = createVector(floor(w / 2), floor(h / 2)); //create a vector element
    this.xdir = 0;
    this.ydir = 0;
    this.len = 0;
  }

  setDir(x, y) {
    this.xdir = x;
    this.ydir = y;
  }

  update() {
    let head = this.body[this.body.length - 1].copy(); //save what is being shifted out of the array
    this.body.shift(); //moves the array over by 1
    head.x += this.xdir;
    head.y += this.ydir;
    this.body.push(head); //push in arrays adds something to them, so this adds what has been shifted out of the array
  }

  grow() {
    let head = this.body[this.body.length - 1].copy();
    this.len++;
    this.body.push(head); //adds something to the end of the array
  }

  endGame() {
    let x = this.body[this.body.length - 1].x;
    let y = this.body[this.body.length - 1].y;
    if (x > w - 1 || x < 0 || y > h - 1 || y < 0) {
      return true;
    }
    for (let i = 0; i < this.body.length - 1; i++) { //-1 so it the head of the snake does not check it selv, but only the rest of the snake
      let part = this.body[i];
      if (part.x == x && part.y == y) { //checks if head of snake touches an other part of the snake
        return true; //the game is over
      }
    }
    return false;
  }

  eat(pos) {
    let x = this.body[this.body.length - 1].x; //makes sure that it is the front square of the snake that eats the food
    let y = this.body[this.body.length - 1].y; //makes sure that it is the front square of the snake that eats the food
    if (x == pos.x && y == pos.y) {
      this.grow();
      return true;
    }
    return false;
  }

  show() {
    for (let i = 0; i < this.body.length; i++) {
      image(sunimg, this.body[i].x, this.body[i].y, 5, 5);
    }
  }
}
