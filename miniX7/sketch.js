let snake;
let rez = 20;
let food;
let w; //to keep track of the world so the snake does not leave it
let h;
let sunimg;
let rainimg;
let cloudimg;
let loseimg;

function preload() {
  sunimg = loadImage('sun.png');
  rainimg = loadImage('rain.png');
  cloudimg = loadImage('cloudgrey.png');
  loseimg = loadImage('losescreen.png');
}

function setup() {
  createCanvas(400, 400);
  w = floor(width / rez); //floor makes it an integer
  h = floor(height / rez);
  frameRate(5);
  snake = new Snake();
  foodLocation();
}

function foodLocation() {
  let x = floor(random(w-10));
  let y = floor(random(h-10));
  food = createVector(x, y);
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    snake.setDir(-1, 0);
  } else if (keyCode === RIGHT_ARROW) {
    snake.setDir(1, 0);
  } else if (keyCode === DOWN_ARROW) {
    snake.setDir(0, 1);
  } else if (keyCode === UP_ARROW) {
    snake.setDir(0, -1);
  } else if (key == ' ') {
    snake.grow();
  }
}

function draw() {
  scale(rez); //scales the snake up and also moves the pixels it changes
  background(cloudimg);

  if (snake.eat(food)) { //If food is eaten then new foodLocation
    foodLocation();
  }
  snake.update();
  snake.show();

  if (snake.endGame()) {
    print('END GAME');
    background(loseimg);
    noLoop();
  }

  noStroke();
  fill(255, 0, 0);
  image(rainimg ,food.x, food.y, 10, 10);
}
