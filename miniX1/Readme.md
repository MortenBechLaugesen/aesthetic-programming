# README for miniX1

[See my code!](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX1/sketch.js)

[See my project!](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX1/)

![](screenshot01.png)

---

### What i have produced:
I have produced a project that tics ten times a second with a piece of text. The text rotates around the mouse pointer and therefore also follows the mousepointer. The translate function of line 15 is what makes the text follow the mousepointers coordinates. The rotation of the text is viable by creating a variable in line 1 and then in line 18-19 placing the variable in a rotate function and stating that the variable will rotate +5 degrees every tic. Before doing this i had to tell the computer what unit of measurement it should use. This is done in line 6 where the syntax angleMode states that it is degrees. 

Other that following the mousepointer and rotating around it, the text also scales in size and changes color as well. The size changing of the text comes from line 19 where the textsize is set to randomise every tic op to a size of 100. The color changing happens only on a greyscale. This was not done intentional, but having tried different ways of getting the color to be random on a RBG scale, it would not work. But visually i believe it is more pleasing and less stressful to look at it on a greyscale. This also helps creating contrast between some yellow text i have in the function setup in line 8-10. This piece of text simply says: “click to clear” and works as an instruction that tells the user that they can do more that just move the mouse around. When the mouse is clicked, everything inside the canvas clears and the rotating text around the mousepointer starts again.


### My first coding experience:
My first independent coding experience was easier that expected. It was easier in the sense that getting into coding and using atom and p5.js was not complicated. Writing code on the other hand was in many instances quite complicated. Understanding what different syntaxes does and how they function together was tricky and sometimes made the code not function. I had a hard time finding references and pieces of code that i could use as solutions to problems. But by spending time discussing these problems with other new coders it helped solve problems. Trial and error was also a major factor in getting the code to work.


### How coding is different or similar to reading and writing text:
Coding differs from writing text by being in the hands of the computer. There is no room for mispelling or forgetting a semicolon etc. The computer does not interpret meanings like when a person reads. A human can read between the lines, a computer needs the exact specifics.


### What code and programming means to me +assigned reading:
For me code and programming does not have a bigger meaning. This might be because i do not have a lot of coding and programming experience. For me code and programming is a tool to create what i or someone else might need. Other than a tool it is also somewhat of a toy for me. With the little code experience i have, coding seems fun (when it works).
The assigned reading helps me reflect on these terms in the sense of how they can function and be used. It also helps me to understand how others think of coding and programming, and how it is useful to them.



[reference](https://www.youtube.com/watch?v=o9sgjuh-CBM&t=370s)
