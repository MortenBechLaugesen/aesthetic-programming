let angle = 0;

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  angleMode(DEGREES);
  frameRate(10);
  background(50);
  textSize(25);
  fill(255,204,0);
  text("click to clear",300,300);
}

function draw() {
  // put drawing code here
  translate(mouseX,mouseY);
  fill(random(0,255));
  rotate(angle);
  angle = angle+5;
  textSize(random(100));
  text("miniX1",0,0);
}

function mousePressed() {
  clear();
  background(50);
}
