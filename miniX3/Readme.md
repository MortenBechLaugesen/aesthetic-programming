# README for miniX3

[See my code!](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX3/sketch.js)

[See my project!](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX3/)

![](screenshotminiX3.png)


---

### What i have produced

I have produced a throbber that is an ellipse moving in a an almost bouning pattern in the middle of a darkgrey canvas.
The throbber is a small teal ellipse moving about dragging a small tail behind it.

Technically the code is short, but helped me get a better understanding of using loops.
Creating this project also helped me get a better understanding of using sin syntax, although I dont fully understand this syntax yet.

Conceptually my hope was to create a throbber that i would find satisfying to look at. I did not look at this project with a more cultural or political look, as opposed to my last miniX. Since the situations in with I incounter throbbers is usualy when watching Netflix or the like, I thought that focusing on the aesthetic and how pleasing it would be to look at was more useful for me.

---

### Time related syntax

The time related syntax i have used is the frameCount syntax. In some of my first itterations of this project, this syntax was needen so the modulo operater calculation could function correctly and make sure the rotation of an ellipse was performed correctly. In my final itteration i have used frameCount as part of my for loop and sin syntax to create the patterns the ellipse moves in. 

---

### Throbbers i encounter in digital culture

Most throbbers i encounter are through different streaming or video services. These throbbers indicate that something is either loading or working in some sense. Usually these throbbers show up more the worst my own connection is. This was also a factor for why i chose to focus on the satisfaction of the throbbers look, since situations where the connection is bad can be frustration, which having something satisfying to look at for the short loading period might be nice.
I also encounter throbbers in instances of downloading or other situations where my computer is not frozen or not working in some sense. The downloading situations are not frustration, so again i thought most of the other situations where satisfaction might help mitigate some frustration for me.

For the most part I look at throbbers as the computer or computer program working on something. I usually just wait till it is done without any further thought as to what the programs are working on. If netflix is slow, a transaction is going through or something is downloading I do not know what the machine is actually doing other that what the final result is. I do not know what data is being read or anything in that sense.

---

### References

[Rainbow infinity throbber](https://openprocessing.org/sketch/1041824)
