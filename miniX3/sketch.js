//infinity throbber
let ballCount = 1;
let ballSize = 30;
let rotdown = 1;
let rotup = 1;


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  noStroke();
  frameRate(60);
}

function draw() {
  fill(60, 100);
  rect(0, 0, width, height);

  // how the throbber moves; sin functions makes shure the throbber stays inside the screen
  for (let i = ballCount; i > 0; i--) {
    fill(i/1.5, 150, 175);
    let posX = (width/2) - sin((frameCount-rotdown)/20) * 120;
    let posY = (height/2) - sin((frameCount-rotup)/10) * 40;
    ellipse(posX, posY, ballSize+i/3, ballSize+i/3);
    rotdown += 0.1
    rotdown += 0.1
  }
}

//canvas scales with the windowsize
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
