### README for miniX5

[See my code here :)](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX5/sketch.js)

[See my project here :)](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX5/)

![](miniX5_6screenshot.png)


---

## Rules

Rule1: If color = yellow --> either blue or red; if color = red or blue --> yellow

Rule2:If color = red --> shape = rectangle

## The roles of rules and processes

Up till this project they didn't really have a role for me, but when creating this project they became the main focus. I think this might have been why I had a hard time implementing the rules and getting them to work. I set some rules and tried different approaches to making it reality, but this just became many attemps that ended up as an amalgamation of different tidbits that didn't work how I wanted. In the end I got it to work with help from my buddygroup.

But, I think that rules and processes might have had a subconcius affect on my earlier project in the sense that where I have learned coding from until now have (I assume) used rules and therefor I have too. This project have showed me the importance of these things and I will definitely focus on improving this in my still flechling stage coding skills.


## The idea of auto-generator

Looking at the assigned reading and esspecialy the Montfort et al. chapter focusing on randomness it has helped me deepen my understanding of auto-generation. In the chapter they show how little code is needed to create something rather expressive and how the use of rules can create something like 10 PRINT rather simply.
Still, even if what can be created easily is complex (at least visualy), I believe that the machine is stil very much reliant on the human behind it. The maching can auto-generate millions and billions of things, but without someone to create rules for this generation it is not possible for the machine. In a sense this using of rules is giving some autonomy away from the programmer since they can do little and get alot, but I don't believe this autonomy is given to the machine.


## References

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.

[For and while loops, the coding train](https://www.youtube.com/watch?v=cnRD9o6odjk&t=656s)

[10 PRINT, the coding traing](https://www.youtube.com/watch?v=bEyTZ5ZZxZs)
