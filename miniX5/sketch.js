/*rules
rule 1:
If color = yellow --> either blue or red; if color = red or blue --> yellow

rule 2:
If color = red --> shape = rectangle
*/

let state = 1; // state 1 = pink, state 2  = blue, state 3 = purple
let gridSpace = 20;
let moveX = 0; //position on the x-axis
let moveY = 0; //position on the y-axis
let bORr;


function setup(){
  createCanvas(windowWidth, windowHeight);
  background(55);
  //grid(); //draw grid
  frameRate(10);
}



function draw() {
  colorChange();
  colorMove();
}


//Rule 1:
function colorChange(){
  if (state==2){
    bORr = random(2);
    if (bORr < 1){
      shapered();
    } else {
      shapeblue();
    }
  } else {
      shapeyellow();

  }
}


function colorMove(){
  moveX = moveX + gridSpace;
  //moveY = moveY+gridSpace
  if(moveX > width){
    moveX = 0;
    moveY = moveY+gridSpace;
  }
}

//rule 2
function shapered(){
  fill(255,100,100); //red
  strokeWeight(4);
  rect(moveX, moveY+gridSpace, gridSpace, gridSpace);
  state = 1;

}
//rule 2
function shapeyellow(){
  stroke(255,255,100); //yellow
  strokeWeight(4);
  line(moveX, moveY-gridSpace, moveX-gridSpace, moveY);
  state = 2;

}
//rule 2
function shapeblue(){
  stroke(100,100,255); //blue
  line(moveX, moveY, moveX+gridSpace, moveY+gridSpace);
  state = 3;

}

//grid
/*function grid(){

  for( let x = 0; x <= width; x += gridSpace){
    for (let y = 0; y <= height; y += gridSpace){
      stroke(55);
      strokeWeight(4);
      fill(100);
      rect(x, y, gridSpace, gridSpace);
    }
  }

}
*/
