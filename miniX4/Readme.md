# README for miniX4

[See my code here :)](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX4/sketch.js)

[See my project here :)](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX4/)

![](screenshotminiX4.png)


---

## (UN)LIKABLE

(UN)LIKABLE is a work revolving arround the worth of social media interaction. This work gives destinct almost valuta like value to general functions of social media. In a society where social media creates daily fame, jobs, interaction, hate, joy, hope, despair and gives and takes value like devine judgement. Without numbering worth, this project projects value on your social media presence and gives your likeability the ability of giving and taking in the form of likes and dislikes. Work hard to climb the social(media) latter or stay forever (UN)LIKABLE.

---

### What I have produced

In this miniX4 assignment i have produced a project that consists simply of three buttons for the user to click on. The two main buttons are a 'like' and 'dislike' button. The third button is a 'clear text' button, this buttons purpose is to clear text on the canvas. The text on the canvas is created when the 'like' and 'dislike' buttons are pressed. When pressing the 'like' button the text " Thank you" appears as a form of confermation that you have pressed the button and to express gratitude over the like. The 'dislike' button when pressed makes the text "You do not have the required likes on your account to dislike this post" appear. This text tells the user that have pressed the dislike that since their account is not likeable enough, then they should not have the ability to decide what is dislikeable. This also serves as a way to subtly nudge the user to either try to get more likes on their account or to not even try and use all the functions.

Throughout the programming face of this project on of the functions I succesfully created was for the console to count whenever the 'like' button was pressed. This I tried to use as an even stronger affermation that for the one pressing these buttons, only the 'like' would count and the 'dislike' wouldn't. This function unfortunatly isn't part of the final iteration of my project, not because I did not want it, but rather that I couldn't get it to work when the function of adding text when pressing the 'like' button. 


(If I at some point goes back to this project with more experience, I'll try making both functions work simultaniously. On the other hand, the truly final version of this project would be as a overarching part of social media platforms, where this would be software that calculates how much your accounts likeability is worth and assigning you function rights (like liking, commenting etc.) on other peoples posts and accounts compared to their accounts likeability - this ofcouse would be quite a distopian function of social media and society.)

---

### What I have used and learnt

In this project I have used mostly the button function of the P5.js library. This is a DOM element that creates a button. Although this is the syntax most used, I have tried using different DOM elements, like sliders, in the process. In the process I have also tried using different data-capture like audio capture, though this didn't become part of my miniX4. I found using data-capture pretty hard and will definately need more working with these functions later on to better get an understanding of how to imlement and use them.

---

### How this adresses the theme "capture all"

When thinking about the theme of "capture all" my project in some ways of looking at it goes away from this. In this instance, only the like button click is captured. But that in a sense is a fallacy since the dislike button click is just captured in another sence; That a person not worthy pressed it, therefore it didn't count as a dislike, but the dislike-attempt is still captured. I believe it would be a struggle for many to avoid the digital capitalism's attempt to 'capture all', but maybe not impossible. Browsers like DuckDuckGo and things like VPNs are attempts at combatting 'capture all'.

My project here looks at how this capture all of digital capitalism might have created a situation where data is value and then maybe likes, dislikes etc. is valuta in the age of digital capitalism.

---

### Cultural implications of data capture

In the documentary with Shoshana Zuboff about surveillance capitalism (https://www.youtube.com/watch?v=hIXhnWUmMvw) one of the most harrowing statements in my oppinion comes at 2:45-3:40. This short part of the ducumentary mentions how practicaly everything about us leaves behind valuable data, and that the data we give up knowingly "is the least important part of the information they collect about us." (Zuboff) What I find especially harrowing about this, is how Zuboff mentions that this data was seen as the only useful data 20 (2000-2002) years ago, where everything else was invaluable waste. This has been flipped intirely on its head and now everything else, all the date we unwitingly give big corporations, is the true moneymaker and the data that can be used to track us, know us and in some instances even control our behavior like with Pokemon Gó (Zuboff at 17:25 in the documentary).

Data capture has become so big a part of society that if someone uses modern technology to some extent, it is infeasible to or maybe even impossible to avoid data capture altogether.

---

### Inspiration

(Black Mirror episode that revolves around social media and mainly social scores)[https://www.imdb.com/title/tt5497778/]
