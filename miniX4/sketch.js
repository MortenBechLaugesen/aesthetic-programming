let clicks1 = 0;


function setup() {
  createCanvas(480,640);
  button1();
  button2();
  clearbutton();
  //button1presscount();

  //button1.mousePressed(button1pressed);
  //button2.mousePressed(button2pressed);
}

function draw() {
  button1.mousePressed(button1pressed);
  button2.mousePressed(button2pressed);
}

//when button1 is pressed it does this
function button1pressed() {

    button1.mousePressed((text)("Thank you", 100, 90));
    button1.mousePressed(counter2);
}

//function button1presscount() {
//  button1.mousePressed(counter2);
//}

//when button2 is pressed it does this
function button2pressed() {
  button2.mousePressed((text)("You do not have the required likes on your account to dislike this post", 100, 290));
}


// clear text button for text clearing
function clearbutton() {
  clearbutton = createButton("clear text");
  clearbutton.mousePressed(clear);
  clearbutton.position(10,200);
  clearbutton.size(50,50);
}

//button1 (like)
function button1() {
  button1 = createButton('like');
  button1.position(100, 100);
  button1.style("color", "#fff");
  button1.style("background", "#2f4f4f");
  button1.size(100,50);
  button1.mouseOut(revertStyle);
  button1.mouseOver(change1)
  //button1.mousePressed(counter2);
  //button1.mousePressed(change1);
}


//button2 (dislike)
function button2() {
  button2 = createButton('dislike');
  button2.position(100, 300);
  button2.style("color", "#fff");
  button2.style("background", "#2f4f4f");
  button2.size(100,50);
  button2.mouseOut(revertStyle);
  button2.mouseOver(change2);
  //button1.mousePressed(counter2);
  //button1.mousePressed(output(0));

}

//button press color change for button1
function change1() {
  button1.style("background", "#778899");
}

//button press color change for button2
function change2() {
  button2.style("background", "#778899");
}

function revertStyle() {
  button1.style("background", "#2f4f4f");
  button2.style("background", "#2f4f4f");
}


//counts in console when button1 pressed
function counter2() {
  clicks1 ++;
  console.log("like", clicks1);
}
