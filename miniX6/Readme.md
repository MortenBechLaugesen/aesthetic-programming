### README for miniX6

[See my code!](https://gitlab.com/MortenBechLaugesen/aesthetic-programming/-/blob/master/miniX6/sketch.js)

[See my project!](https://MortenBechLaugesen.gitlab.io/aesthetic-programming/miniX6/)

![](miniX6screenshot.png)


---

### What I reworked

I have reworked my miniX3 throbber. The changes aren't big, but I changed the movement of the throbber. This change was done in acordance to the feedback I got on miniX3, where the feedbackers found the design "somewhat frustrating"(from feedback) since it looks like the throbber is moving in an infinity shape, but it turns out it doesn't; it's movements are more sporatic that such - "slightly unpredictable and creates a strange uncertainty."(from feedback) I more or less kept the things about the throber that the feedback finds less stressfull: the muted colors and non-busy look.

---

I think aesthetic programming in my work here comes to play when the people who gave feedback found the original throbber design somewhat frustrating. In my miniX3 I write that throbbers often show up on my computer when my internet connection is bad. Throbbers for me are a necessary evil that shows the technology working. The throbber beeing frustrating might make the already frustrating situation of waiting for something (which the throbber indicates) more frustrating. In this itteration of the throbber, it moves in the shape of an infinity which is more pleasing and takes into acount the situation of when a throbber is on ones computer. 

---

The coding in this project isn't complicated or technocaly advanced. On the other hand it isn't just coding, there are many thoughts going into the project; As stated earlier, the situation of the throbber, the feelings of users and more. Programming can be more than just coding and using coding as a practice or as a method for design has to take into account more than just the code itself. When coding as part of design, it might be necessary to take socio political situations into account. There might need to be more to the design that code; Like visual, thoughprovoking or thoughout interactivity.

---

Digital culture has shaped a lot of porgramming and the other way around. From big tech that have created multifunctional platforms for sharing and shaping digital culture, to individuals using programming as a means to showcase their thoughts and feelings or just creating digital ecosystems by programming, sites, artwork, bots and much more.

---


Critical making is not just technological evolution, it also draws on cultural evolution and the combination and linking of the two. It is necessary to include the conceptual in the tecnical work in critical making.

> The argument the book follows is that computational culture is not just a trendy topic to study to improve problem-solving and analytical skills, or a way to understand more about what is happening with computational processes, but is a means to engage with programming to question existing technological paradigms and further create changes in the technical system. We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities. (Soon & Cox, 2020, p. 14)

This above quote is a good example of why it is a good practice to take more than just the technical into account. Without reflection on one's own creations as well as other's creations it is harder to understand and better what has been created.

I believe that aesthetic programming is a means to question technology as well as the culture that is part of the technology. With a better understanding of the digital culture I think programming and critical making can be used more effectively as tools in digital culture to further evolve what is being created by understanding the world around us better. Technology influences us more and more, so being able to look criticaly at it gives both creater and user a better understanding of our own lives.


### References

- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020

- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28
